/**
 * Webpack config
 * Created by Sergio A Guzman <sagg629@gmail.com> on 26/07/16 03:04 PM
 */
module.exports = {
    entry: './src', //files to be application's entry point
    output: {
        path: 'builds', //compile our bundle in /builds dir
        filename: 'bundle.js', //... under the name bundle.js
        publicPath: 'builds/', //tell Webpack where to find built <<assets from the point of view of the page>>
        chunkFilename: '[name].bundle.js' //name to all chunks
    },
    module: {
        //specify loaders for different file-types
        loaders: [
            {
                //babel to run over ALL .js files under /src dir (excluding dependencies and others)
                test: /\.js/,
                loader: 'babel',
                include: __dirname + '/src'
            },
            {
                test: /\.scss/,
                loaders: ['style', 'css', 'sass'] //also loader: 'style!css!sass'
            },
            {
                test: /\.html/,
                loader: 'html'
            }
        ]
    },
    devServer: {
        hot: true
    }
};
