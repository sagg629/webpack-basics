/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 27/07/16 01:23 PM
 */
import $ from "jquery";
import template from "./header.html";
import Mustache from "mustache";
import "./header.scss";

export default class Header {
    constructor(link) {
        this.link = link;
    }
    
    render(node) {
        const text = $(node).text();
        // Render our header
        $(node).html(
            Mustache.render(template, {text})
        );
    }
}
