/**
 * Code for button
 * Created by Sergio A Guzman <sagg629@gmail.com> on 26/07/16 03:55 PM
 */
import $ from "jquery";
import template from "./button.html";
import Mustache from "mustache";
import "./button.scss";

export default class Button {
    constructor(link) {
        this.link = link;
    }

    onClick(event) {
        event.preventDefault();
        alert(this.link);
    }

    render(node) {
        const text = $(node).text();
        // Render our button
        $(node).html(
            Mustache.render(template, {text})
        );
        // Attach our listeners
        $('.button').click(this.onClick.bind(this));
    }
}
