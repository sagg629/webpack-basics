/**
 * App's entry point
 *
 * var $ = require('jquery'); //ES5-based loading
 * import $ from "jquery";    //ES6-based loading
 *
 * Load desired components DYNAMICALLY with require.ensure()
 *
 * Created by Sergio A Guzman <sagg629@gmail.com> on 26/07/16 03:03 PM
 */
if (document.querySelectorAll('a').length) { //if there's an <a> tag in html ...
    require.ensure([], () => {
        //everything here will be splitted into a <<chunk>>,
        //that is a separate bundle that Webpack will load ONLY when needed (when this JS / section) has been queried
        const Button = require('./components/button/button').default;
        const btn = new Button('te la creiste we :v');
        btn.render('a');
    }, 'button');
}
if (document.querySelectorAll('h1').length) {
    require.ensure([], ()=> {
        const Header = require('./components/header/header').default;
        new Header("este es el header").render('h1');
    }, 'h1');
}
//import Button from "./components/button";
//const btn = new Button('te la creiste we :v');
//btn.render('a');
